/********************************************************************
 * File: analyse.cc
 * ------------------------
 *
 * Description:
 * Analyse something.
 *
 *
 * Version:
 * Author: Florian Pitters
 *
 *******************************************************************/

#include "analysis.h"


int analyse(){

    styleCommon();
    styleMap();
    styleGraph();

    double cal;
    std::string out_file;

    Color_t kOneBlue = (new TColor(0/256., 43/256., 128/256.))->GetNumber();
    Color_t kOneMagenta = (new TColor(199/256., 68/256., 24/256.))->GetNumber();
    Color_t kOneOrange = (new TColor(221/256., 138/256., 13/256.))->GetNumber();
    Color_t kOneGreen = (new TColor(81/256., 153/256., 28/256.))->GetNumber();
    Color_t kOneCyan = (new TColor(24/256., 156/256., 199/256.))->GetNumber();
    Color_t kOneRed = (new TColor(109/256., 15/256., 14/256.))->GetNumber();

    Color_t kOneAzure = (new TColor(0/256., 153/256., 153/256.))->GetNumber();
    Color_t kOneOrange2 = (new TColor(221/256., 162/256., 13/256.))->GetNumber();
    Color_t kOneBlue2 = (new TColor(35/256., 75/256., 133/256.))->GetNumber();

    std::vector<Marker_t> markers = {kFullSquare, kFullCircle, kFullTriangleUp, kFullTriangleDown, kFullDiamond, kFullCrossX, kFullDoubleDiamond};
    std::vector<Style_t> styles = {1, 2, 4, 6, 7, 10};
    std::vector<Color_t> colours = {kOneBlue, kOneMagenta, kOneOrange, kOneGreen, kOneCyan, kOneOrange};


    auto res_test = std::make_shared<runResults> ();
    
    // lab tests
    // res_test = analyse_series(dat_path, "setup0_labor_laser", "laser_50V_tune10", "laser");
    res_test = analyse_series(dat_path, "setup0_labor_laser", "laser_75V_tune10", "laser");
    // res_test = analyse_series(dat_path, "setup0_labor_trigger", "tune90_40MHz_400mVdiv", "trigger");
    // res_test = analyse_parallel(dat_path, "setup0_labor_trigger", "tune90_40MHz_400mVdiv", "trigger");
    // res_test = analyse_parallel(dat_path, "setup0_labor_channels", "laser_350V_10mVdiv_tune99_3-4", "laser");

    // setup 5 carbons
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_120MeV_150V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_120MeV_200V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_120MeV_250V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_120MeV_300V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_200MeV_200V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_200MeV_225V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_200MeV_250V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_200MeV_275V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_200MeV_300V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_300MeV_200V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_300MeV_250V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_400MeV_200V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_400MeV_225V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_400MeV_250V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_400MeV_275V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_400MeV_300V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_400MeV_325V", "carbons");
    // res_test = analyse_parallel(dat_path, "setup5_carbons", "setup5_carbons_400MeV_350V", "carbons");

    // setup 5 protons
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_62MeV_300V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_62MeV_325V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_62MeV_350V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_83MeV_300V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_83MeV_325V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_83MeV_350V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_100MeV_200V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_100MeV_225V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_100MeV_250V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_100MeV_275V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_100MeV_300V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_100MeV_325V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_100MeV_350V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_145MeV_300V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_145MeV_325V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_145MeV_350V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_194MeV_300V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_194MeV_325V", "protons");
    res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_194MeV_350V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_250MeV_200V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_250MeV_250V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_250MeV_275V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_250MeV_300V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_250MeV_325V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_250MeV_350V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_800MeV_300V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_800MeV_325V", "protons");
    // res_test = analyse_parallel(dat_path, "setup5_protons", "setup5_protons_800MeV_350V", "protons");
    
    // setup 6 bandwidth
    // res_test = analyse_parallel(dat_path, "setup6_bandwidth", "setup6_194MeV_300V_1GHz_10mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_bandwidth", "setup6_194MeV_325V_1GHz_10mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_bandwidth", "setup6_194MeV_350V_1GHz_10mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_bandwidth", "setup6_194MeV_350V_05GHz_10mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_bandwidth", "setup6_194MeV_350V_2GHz_10mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_bandwidth", "setup6_194MeV_350V_4GHz_10mV", "protons");    
    // res_test = analyse_parallel(dat_path, "setup6_bandwidth", "setup6_194MeV_350V_6GHz_10mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_bandwidth", "setup6_194MeV_350V_8GHz_10mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_bandwidth", "setup6_194MeV_350V_8GHz_HW_10mV", "protons");
    
    
    // setup 6 protons
    // res_test = analyse_parallel(dat_path, "setup6_protons", "setup6_100MeV_360V_1GHz_10mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_protons", "setup6_100MeV_360V_05GHz_10mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_protons", "setup6_100MeV_370V_1GHz_10mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_protons", "setup6_100MeV_370V_05GHz_10mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_protons", "setup6_194MeV_360V_1GHz_10mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_protons", "setup6_194MeV_360V_05GHz_10mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_protons", "setup6_194MeV_365V_1GHz_20mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_protons", "setup6_194MeV_370V_1GHz_20mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_protons", "setup6_194MeV_370V_05GHz_20mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_protons", "setup6_194MeV_375V_1GHz_20mV", "protons");
    
    // setup 6 sampling
    // res_test = analyse_parallel(dat_path, "setup6_sampling", "setup6_194MeV_360V_1GHz_10mV", "protons");
    // res_test = analyse_parallel(dat_path, "setup6_sampling", "setup6_194MeV_360V_05GHz_10mV", "protons");
    
    
    
    // Raw
    // -----------------------
    
    std::vector<double> c_bias_120 = {150, 200, 250, 300};
    std::vector<double> c_dt45_120 = {0.0532, 0.0553, 0.0568, 0.0578};
    std::vector<double> c_dt45_120_alt = {0.0317, 0.0315, 0.0325, 0.034};
    std::vector<double> c_tof45_120 = {0.0452, -0.1372, -0.0845, 0.0668};
    std::vector<double> c_charge_120 = {-1, -1, -1, -1};
    std::vector<double> c_mpv_120 = {2.1, 2.3, 2.8, 3.3};
    std::vector<double> c_sn_120 = {87.9, 104.4, 120.4, 140.4};
    std::vector<double> c_trise_120 = {0.5296, 0.5394, 0.5403, 0.5369};
    std::vector<double> c_trise_var_120 = {0.03, 0.03, 0.03, 0.03};
    
    std::vector<double> c_bias_200 = {200, 225, 250, 275, 300};
    std::vector<double> c_dt45_200 = {0.0546, 0.0549, 0.0558, 0.0570, 0.0596};
    std::vector<double> c_dt45_200_alt = {0.0321, 0.0327, 0.0337, 0.0349, 0.0358};
    std::vector<double> c_tof45_200 = {0.1313, -0.0499, -0.0043, -0.0165, 0.1160};
    std::vector<double> c_charge_200 = {-1, -1, -1, -1, -1};
    std::vector<double> c_mpv_200 = {2.4, 2.6, 2.8, 2.9, 3.1};
    std::vector<double> c_sn_200 = {77.7, 84.5, 92.1, 99.8, 108.3};
    std::vector<double> c_trise_200 = {0.556, 0.553, 0.550, 0.548, 0.544};
    std::vector<double> c_trise_var_200 = {0.03, 0.03, 0.03, 0.03, 0.03};
    
    std::vector<double> c_bias_300 = {200, 250};
    std::vector<double> c_dt45_300 = {0.0544, 0.0536};
    std::vector<double> c_dt45_300_alt = {0.0344, 0.0340};
    std::vector<double> c_tof45_300 = {0.0048, 0.0117};
    std::vector<double> c_charge_300 = {-1, -1};
    std::vector<double> c_mpv_300 = {2.0, 2.2};
    std::vector<double> c_sn_300 = {64.7, 76.5};
    std::vector<double> c_trise_300 = {0.5675, 0.559};
    std::vector<double> c_trise_var_300 = {0.03, 0.03};
    
    std::vector<double> c_bias_400 = {200, 225, 250, 275, 300, 325, 350};
    std::vector<double> c_dt45_400 = {0.0536, 0.0542, 0.0551, 0.0568, 0.0591, 0.0606, 0.0625};
    std::vector<double> c_dt45_400_alt = {0.0341, 0.0344, 0.0352, 0.0362, 0.0376, 0.0387, 0.0405};
    std::vector<double> c_tof45_400 = {0.0196, 0.0066, -0.0252, 0.0102, -0.0062, 0.0528, 0.0196};
    std::vector<double> c_charge_400 = {-1, -1, -1, -1, -1, -1};
    std::vector<double> c_mpv_400 = {1.7, 1.9, 2.0, 2.1, 2.2, 2.3, 2.4};
    std::vector<double> c_sn_400 = {58.6, 66, 71, 77, 79, 82, 85};
    std::vector<double> c_trise_400 = {0.584, 0.567, 0.561, 0.554, 0.549, 0.542, 0.5369};
    std::vector<double> c_trise_var_400 = {0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03};

    std::vector<double> p_bias_62 = {300, 325, 350};
    std::vector<double> p_dt45_62 = {0.0746, 0.0722, 0.0720};
    std::vector<double> p_dt45_62_alt = {0.0434, 0.0408, 0.0404};
    std::vector<double> p_tof45_62 = {-0.1702, -0.1161, -0.1196};
    std::vector<double> p_charge_62 = {};
    std::vector<double> p_mpv_62 = {};
    std::vector<double> p_sn_62 = {60.7, 66.0, 71.3};
    std::vector<double> p_trise_62 = {0.526, 0.517, 0.511};
    std::vector<double> p_trise_var_62 = {0.0265, 0.0241, 0.0224};

    std::vector<double> p_bias_83 = {300, 325, 350};
    std::vector<double> p_dt45_83 = {0.0755, 0.0747, 0.0718};
    std::vector<double> p_dt45_83_alt = {0.0429, 0.0423, 0.0400};
    std::vector<double> p_tof45_83 = {-0.1137, -0.1150, -0.1017};
    std::vector<double> p_charge_83 = {};
    std::vector<double> p_mpv_83 = {};
    std::vector<double> p_sn_83 = {50.6, 57.3, 62.2};
    std::vector<double> p_trise_83 = {0.522, 0.513, 0.501};
    std::vector<double> p_trise_var_83 = {0.0280, 0.0249, 0.0269};

    std::vector<double> p_bias_100 = {200, 225, 250, 275, 300, 325, 350, 360, 370};
    std::vector<double> p_dt45_100 = {0.0884, 0.0865, 0.0807, 0.0772, 0.0758, 0.0744, 0.0722, 0.0752, 0.0745};
    std::vector<double> p_dt45_100_alt = {0.0568, 0.0509, 0.0494, 0.0455, 0.0435, 0.0421, 0.0405, -1, -1};
    std::vector<double> p_tof45_100 = {-0.0662, -0.0455, -0.0143, -0.0995, -0.0944, -0.0935, -0.0911, -1, -1};
    std::vector<double> p_charge_100 = {};
    std::vector<double> p_mpv_100 = {};
    std::vector<double> p_sn_100 = {26.2, 30.3, 34.3, 39.9, 45.1, 51.3, 57.0, -1, -1};
    std::vector<double> p_trise_100 = {0.5807, 0.5618, 0.5454, 0.5310, 0.5211, 0.5111, 0.5051, -1, -1};
    std::vector<double> p_trise_var_100 = {0.0582, 0.0492, 0.0415, 0.0347, 0.0307, 0.0262, 0.0247, -1, -1};
    
    std::vector<double> p_bias_145 = {300, 325, 350};
    std::vector<double> p_dt45_145 = {0.0811, 0.0765, 0.0724};
    std::vector<double> p_dt45_145_alt = {0.0472, 0.0427, 0.0399};
    std::vector<double> p_tof45_145 = {-0.0605, -0.0047, -0.0746};
    std::vector<double> p_charge_145 = {};
    std::vector<double> p_mpv_145 = {};
    std::vector<double> p_sn_145 = {36.6, 42.4, 48.0};
    std::vector<double> p_trise_145 = {0.5169, 0.5070, 0.4980};
    std::vector<double> p_trise_var_145 = {0.0375, 0.0294, 0.0259};
    
    std::vector<double> p_bias_194 = {300, 325, 350, 360, 370};
    std::vector<double> p_dt45_194 = {0.0834, 0.0778, 0.0729, 0.0736, 0.0732};
    std::vector<double> p_dt45_194_alt = {0.0467, 0.0437, 0.0397, -1, -1};
    std::vector<double> p_tof45_194 = {0.0032, 0.0173, -0.0532, -1, -1};
    std::vector<double> p_charge_194 = {};
    std::vector<double> p_mpv_194 = {};
    std::vector<double> p_sn_194 = {31.3, 36.2, 41.4, -1, -1};
    std::vector<double> p_trise_194 = {0.5125, 0.5041, 0.5111, -1, -1};
    std::vector<double> p_trise_var_194 = {0.0386, 0.0342, 0.0301, -1, -1};
    
    
    std::vector<double> p_bias_250 = {200, 225, 250, 275, 300, 325, 350};
    std::vector<double> p_dt45_250 = {-1, -1, 0.1021, 0.0931, 0.0868, 0.0802, 0.0755};
    std::vector<double> p_dt45_250_alt = {-1, -1, 0.0623, 0.0530, 0.0490, 0.0437, 0.0415};
    std::vector<double> p_tof45_250 = {-1, -1, -0.0486, -0.0188, -0.0018, 0.0193, -0.0188};
    std::vector<double> p_charge_250 = {};
    std::vector<double> p_mpv_250 = {};
    std::vector<double> p_sn_250 = {-1, -1, 22.5, 23.7, 27.0, 32.1, 37.9};
    std::vector<double> p_trise_250 = {0.584, -1, 0.536, 0.520, 0.510, 0.501, 0.491};
    std::vector<double> p_trise_var_250 = {0.835, -1, 0.0575, 0.0503, 0.0428, 0.0398, 0.0320};
    
    std::vector<double> p_bias_800 = {300, 325, 350};
    std::vector<double> p_dt45_800 = {0.138, 0.1038, 0.0901};
    std::vector<double> p_dt45_800_alt = {-1, 0.1074, 0.0482};
    std::vector<double> p_tof45_800 = {-0.2073, -0.1214, 0.0720};
    std::vector<double> p_charge_800 = {0.127};
    std::vector<double> p_mpv_800 = {};
    std::vector<double> p_sn_800 = {15.7, 21.5, 24.7};
    std::vector<double> p_trise_800 = {0.5049, 0.4920, 0.4828};
    std::vector<double> p_trise_var_800 = {0.0693, 0.0652, 0.0606};


    for (auto &v : p_dt45_62){ v *= 1./sqrt(2); }
    for (auto &v : p_dt45_83){ v *= 1./sqrt(2); }
    for (auto &v : p_dt45_100){ v *= 1./sqrt(2); }
    for (auto &v : p_dt45_145){ v *= 1./sqrt(2); }
    for (auto &v : p_dt45_194){ v *= 1./sqrt(2); }
    for (auto &v : p_dt45_250){ v *= 1./sqrt(2); }
    for (auto &v : p_dt45_800){ v *= 1./sqrt(2); }

    for (auto &v : c_dt45_120){ v *= 1./sqrt(2); }
    for (auto &v : c_dt45_200){ v *= 1./sqrt(2); }
    for (auto &v : c_dt45_300){ v *= 1./sqrt(2); }
    for (auto &v : c_dt45_400){ v *= 1./sqrt(2); }
    
    
    
    // Summary
    // -----------------------
    
    std::vector<double> zero_err = {0,0,0,0,0,0,0,0,0,0,0};
    
    std::vector<double> c_energy = {120, 200, 300, 400};
    std::vector<double> p_energy = {62, 83, 100, 145, 194, 250, 800};
    std::vector<double> c_bias = {150, 200, 225, 250, 275, 300, 325, 350};
    std::vector<double> p_bias = {200, 225, 250, 275, 300, 325, 350};

    std::vector<double> p_trise_vs_bias = {-1, -1, -1, -1, -1, -1,-1, -1};
    std::vector<double> c_trise_vs_bias = {-1, -1, -1, -1, -1, -1,-1, -1};
    std::vector<double> p_amp_vs_bias = {0.0078, 0.0091, 0.0106, 0.0121, 0.0141, 0.0166, 0.0195};
    std::vector<double> c_amp_vs_bias = {-1, -1, -1, -1, -1, -1,-1, -1};
    std::vector<double> p_charge_vs_bias = {0.2008, 0.2263, 0.2554, 0.2869, 0.3312, 0.3844, 0.4542};
    std::vector<double> c_charge_vs_bias = {-1, -1, -1, -1, -1, -1,-1, -1};

    std::vector<double> c_amp_vs_ene = {-1, -1,- 1, -1};
    std::vector<double> p_amp_vs_ene = {0.0244, 0.0215, 0.0195, 0.0162, 0.0140, 0.0125, 0.0089};
    std::vector<double> c_charge_vs_ene = {-1, -1,- 1, -1};
    std::vector<double> p_charge_vs_ene = {0.5826, 0.5042, 0.4542, 0.3697, 0.3180, 0.2814, 0.1794};
    
    std::vector<double> p_dt_vs_ene_300V = {p_dt45_62.at(0), p_dt45_83.at(0), p_dt45_100.at(4), 
        p_dt45_145.at(0), p_dt45_194.at(0), p_dt45_250.at(4), p_dt45_800.at(0)};
    std::vector<double> p_dt_vs_ene_325V = {p_dt45_62.at(1), p_dt45_83.at(1), p_dt45_100.at(5), 
        p_dt45_145.at(1), p_dt45_194.at(1), p_dt45_250.at(5), p_dt45_800.at(1)};
    std::vector<double> p_dt_vs_ene_350V = {p_dt45_62.at(2), p_dt45_83.at(2), p_dt45_100.at(6), 
        p_dt45_145.at(2), p_dt45_194.at(2), p_dt45_250.at(6), p_dt45_800.at(2)};
    std::vector<double> c_dt_vs_ene_200V = {c_dt45_120.at(0), c_dt45_200.at(0), c_dt45_300.at(0), 
        c_dt45_400.at(0)};    
    std::vector<double> c_dt_vs_ene_250V = {c_dt45_120.at(2), c_dt45_200.at(2), c_dt45_300.at(1), 
        c_dt45_400.at(2)};

    std::vector<double> p_tof_vs_ene_300V = {p_tof45_62.at(0), p_tof45_83.at(0), p_tof45_100.at(4), 
        p_tof45_145.at(0), p_tof45_194.at(0), p_tof45_250.at(4), p_tof45_800.at(0)};
    std::vector<double> p_tof_vs_ene_325V = {p_tof45_62.at(1), p_tof45_83.at(1), p_tof45_100.at(5), 
        p_tof45_145.at(1), p_tof45_194.at(1), p_tof45_250.at(5), p_tof45_800.at(1)};
    std::vector<double> p_tof_vs_ene_350V = {p_tof45_62.at(2), p_tof45_83.at(2), p_tof45_100.at(6), 
        p_tof45_145.at(2), p_tof45_194.at(2), p_tof45_250.at(6), p_tof45_800.at(2)};
    std::vector<double> c_tof_vs_ene_200V = {c_tof45_120.at(0), c_tof45_200.at(0), c_tof45_300.at(0), 
        c_tof45_400.at(0)};    
    std::vector<double> c_tof_vs_ene_250V = {c_tof45_120.at(2), c_tof45_200.at(2), c_tof45_300.at(1), 
        c_tof45_400.at(2)};


    // Labor
    // -----------------------

    // gain
    std::vector<double> l_volts = {50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 310, 
        320, 330, 340, 350, 360, 370, 380, 390};
    std::vector<double> l_amp = {15.9, 27.0, 36.4, 45.2, 54.3, 63.8, 74.9, 86.6, 103, 124, 153, 173, 
        191, 213, 247, 282, 310, 319, 380};
    std::vector<double> l_charge = {1.08, 1.19, 1.35, 1.48, 1.64, 1.82, 2.05, 2.35, 2.71, 3.19, 3.86, 
        4.33, 4.75, 5.26, 6.06, 6.85, 7.73, 8.93, 13.8, 24.3};
        
    // saturation 
    std::vector<double> l_intensity = {1, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
    std::vector<double> l_charge_1st = {20, 30, 43, 56, 69, 82, 94, 105, 116, 126, 131};
    std::vector<double> l_charge_2nd = {23, 35, 40, 42, 43.3, 43.6, 44.2, 44.4, 44.6, 44.8, 45.1};
        
        

    // Theory
    // -----------------------
        
    std::vector<double> p_dep_vs_ene = {8.352E+00, 6.698E+00, 5.838E+00, 4.486E+00, 3.699E+00, 3.165E+00, 1.899E+00};
    std::vector<double> c_dep_vs_ene = {-1, -1, -1, -1};
    
    std::vector<double> p_time_vs_ene = {0.2407, 0.2112, 0.1948, 0.1669, 0.1490, 0.1359, 0.0991};
    std::vector<double> c_time_vs_ene = {0.1798, 0.1469, 0.1275, 0.1167};
    
    std::vector<double> p_dep_mpv_vs_ene = {21.75, 16.83, 14.36, 10.05, 8.43, 7.05, 3.78}; // [ke]
    std::vector<double> p_dep_width_vs_ene = {1.83, 1.51, 1.28, 1.03, 0.86, 0.74, 0.44};
    std::vector<double> h_dep_mpv_vs_ene = {54.2, 36.7, 27.8, 23.3};
    std::vector<double> h_dep_width_vs_ene = {3.4, 2.7, 2.3, 1.8};
    std::vector<double> c_dep_mpv_vs_ene = {536, 370, 283, 147};
    std::vector<double> c_dep_width_vs_ene = {29, 24, 17, 12};
    
    std::vector<double> p_dep_ratio_vs_ene = {21.75/1.83, 16.83/1.51, 14.36/1.28, 10.05/1.03, 8.43/0.86, 
        7.05/0.74, 3.78/0.44};
    std::vector<double> h_dep_ratio_vs_ene = {54.2/3.4, 36.7/2.7, 27.8/2.3, 23.3/1.8};
    std::vector<double> c_dep_ratio_vs_ene = {536/29., 370/24., 283/17., 147/12.};

    // out_file = res_path + "tof_rms.pdf";
    // auto canv_tof = std::make_shared<TCanvas>("tof", "tof");
    // auto mg_tof = std::make_shared<TMultiGraph>();
    // auto lg_tof = std::make_shared<TLegend>(0.70, 0.735, 0.87, 0.87);
    // addGraph(mg_tof, lg_tof, energy.size(), &energy[0], &dt30_rms[0], colours.at(2),
    //     markers.at(2), 1.5, styles.at(2), "APC", "dt30");
    // addGraph(mg_tof, lg_tof, energy.size(), &energy[0], &dt45_rms[0], colours.at(1),
    //     markers.at(1), 1.5, styles.at(2), "APC", "dt45");
    // addGraph(mg_tof, lg_tof, energy.size(), &energy[0], &dt60_rms[0], colours.at(0),
    //     markers.at(0), 1.5, styles.at(2), "APC", "dt60");
    // mg_tof->Draw("APC");
    // mg_tof->GetXaxis()->SetTitle("beam energy [MeV]");
    // mg_tof->GetYaxis()->SetTitle("tof rms [ns]");
    // mg_tof->GetYaxis()->SetTitleOffset(1.2);
    // mg_tof->GetXaxis()->SetLimits(-10, 350);
    // mg_tof->SetMinimum(0.00);
    // mg_tof->SetMaximum(0.12);
    // lg_tof->Draw();
    // canv_tof->SaveAs(out_file.c_str());


    // protons: charge vs energy
    out_file = res_path + "p_charge_vs_ene.pdf";
    auto canv_p_charge_vs_ene = std::make_shared<TCanvas>("p_charge_vs_ene", "p_charge_vs_ene");
    auto mg_p_charge_vs_ene = std::make_shared<TMultiGraph>();
    auto lg_p_charge_vs_ene = std::make_shared<TLegend>(0.65, 0.78, 0.87, 0.87);
    cal = 0.3180/1.899;
    for (auto &v : p_dep_vs_ene){
        v *= cal;
    }
    addGraph(mg_p_charge_vs_ene, lg_p_charge_vs_ene, p_energy.size(), &p_energy[0], &p_charge_vs_ene[0], 
        colours.at(1), markers.at(1), 1.2, styles.at(2), "AP", "measured");
    addGraph(mg_p_charge_vs_ene, lg_p_charge_vs_ene, p_energy.size(), &p_energy[0], &p_dep_vs_ene[0],      
        colours.at(0), markers.at(0), 1.2, styles.at(2), "AP", "theory");
    mg_p_charge_vs_ene->Draw("APL");
    mg_p_charge_vs_ene->GetXaxis()->SetTitle("beam energy [MeV]");
    mg_p_charge_vs_ene->GetYaxis()->SetTitle("mean pulse area [mV*ns]");
    mg_p_charge_vs_ene->GetYaxis()->SetTitleOffset(1.2);
    mg_p_charge_vs_ene->GetXaxis()->SetLimits(0, 850);
    mg_p_charge_vs_ene->SetMinimum(0.00);
    mg_p_charge_vs_ene->SetMaximum(0.85);
    lg_p_charge_vs_ene->Draw();
    canv_p_charge_vs_ene->SaveAs(out_file.c_str());
    
    // protons: amp vs energy
    out_file = res_path + "p_amp_vs_ene.pdf";
    auto canv_p_amp_vs_ene = std::make_shared<TCanvas>("p_amp_vs_ene", "p_amp_vs_ene");
    auto mg_p_amp_vs_ene = std::make_shared<TMultiGraph>();
    auto lg_p_amp_vs_ene = std::make_shared<TLegend>(0.65, 0.78, 0.87, 0.87);
    cal = (1.899/0.3180)*0.0089/1.9;
    for (auto &v : p_dep_vs_ene){
        v *= cal;
    }
    addGraph(mg_p_amp_vs_ene, lg_p_amp_vs_ene, p_energy.size(), &p_energy[0], &p_amp_vs_ene[0], 
        colours.at(1), markers.at(1), 1.2, styles.at(2), "AP", "measured");
    addGraph(mg_p_amp_vs_ene, lg_p_amp_vs_ene, p_energy.size(), &p_energy[0], &p_dep_vs_ene[0],      
        colours.at(0), markers.at(0), 1.2, styles.at(2), "AP", "theory");
    // mg_mg->Draw("A CP PLC PMC PFC");
    mg_p_amp_vs_ene->Draw("APL");
    mg_p_amp_vs_ene->GetXaxis()->SetTitle("beam energy [MeV]");
    mg_p_amp_vs_ene->GetYaxis()->SetTitle("mean pulse height [mV]");
    mg_p_amp_vs_ene->GetYaxis()->SetTitleOffset(1.2);
    mg_p_amp_vs_ene->GetXaxis()->SetLimits(0, 850);
    // mg_p_amp_vs_ene->SetMinimum(0.00);
    // mg_p_amp_vs_ene->SetMaximum(0.85);
    lg_p_amp_vs_ene->Draw();
    canv_p_amp_vs_ene->SaveAs(out_file.c_str());

    out_file = res_path + "p_charge_vs_bias.pdf";
    auto canv_p_charge_vs_bias = std::make_shared<TCanvas>("p_charge_vs_bias", "p_charge_vs_bias");
    auto mg_p_charge_vs_bias = std::make_shared<TMultiGraph>();
    auto lg_p_charge_vs_bias = std::make_shared<TLegend>(0.65, 0.78, 0.87, 0.87);
    addGraph(mg_p_charge_vs_bias, lg_p_charge_vs_bias, p_bias.size(), &p_bias[0], &p_charge_vs_bias[0], 
        colours.at(1), markers.at(1), 1.2, styles.at(2), "AP", "measured");
    // addGraph(mg_p_charge_vs_bias, lg_p_charge_vs_bias, bias.size(), &bias[0], &p_dep_vs_bias[0],      
    //     colours.at(0), markers.at(0), 1.2, styles.at(2), "AP", "theory");
    mg_p_charge_vs_bias->Draw("APL");
    mg_p_charge_vs_bias->GetXaxis()->SetTitle("bias voltage (V)");
    mg_p_charge_vs_bias->GetYaxis()->SetTitle("mean pulse area [mV*ns]");
    mg_p_charge_vs_bias->GetYaxis()->SetTitleOffset(1.2);
    mg_p_charge_vs_bias->GetXaxis()->SetLimits(190, 360);
    mg_p_charge_vs_bias->SetMinimum(0.00);
    mg_p_charge_vs_bias->SetMaximum(0.6);
    lg_p_charge_vs_bias->Draw();
    canv_p_charge_vs_bias->SaveAs(out_file.c_str());

    out_file = res_path + "p_amp_vs_bias.pdf";
    auto canv_p_amp_vs_bias = std::make_shared<TCanvas>("p_amp_vs_bias", "p_amp_vs_bias");
    auto mg_p_amp_vs_bias = std::make_shared<TMultiGraph>();
    auto lg_p_amp_vs_bias = std::make_shared<TLegend>(0.65, 0.78, 0.87, 0.87);
    addGraph(mg_p_amp_vs_bias, lg_p_amp_vs_bias, p_bias.size(), &p_bias[0], &p_amp_vs_bias[0], 
        colours.at(1), markers.at(1), 1.2, styles.at(2), "AP", "measured");
    // addGraph(mg_p_amp_vs_bias, lg_p_amp_vs_bias, bias.size(), &bias[0], &p_dep_vs_bias[0],      
    //     colours.at(0), markers.at(0), 1.2, styles.at(2), "AP", "theory");
    mg_p_amp_vs_bias->Draw("APL");
    mg_p_amp_vs_bias->GetXaxis()->SetTitle("bias voltage (V)");
    mg_p_amp_vs_bias->GetYaxis()->SetTitle("mean pulse height [mV]");
    mg_p_amp_vs_bias->GetYaxis()->SetTitleOffset(1.2);
    mg_p_amp_vs_bias->GetXaxis()->SetLimits(190, 360);
    mg_p_amp_vs_bias->SetMinimum(0.00);
    mg_p_amp_vs_bias->SetMaximum(0.05);
    lg_p_amp_vs_bias->Draw();
    canv_p_amp_vs_bias->SaveAs(out_file.c_str());
    
    out_file = res_path + "p_dt_vs_ene.pdf";
    auto canv_p_dt_vs_ene = std::make_shared<TCanvas>("p_dt_vs_ene", "p_dt_vs_ene");
    auto mg_p_dt_vs_ene = std::make_shared<TMultiGraph>();
    auto lg_p_dt_vs_ene = std::make_shared<TLegend>(0.65, 0.735, 0.87, 0.87);
    addGraph(mg_p_dt_vs_ene, lg_p_dt_vs_ene, p_energy.size(), &p_energy[0], &p_dt_vs_ene_300V[0], 
        colours.at(2), markers.at(2), 1.5, styles.at(2), "AP", "300V bias");
    addGraph(mg_p_dt_vs_ene, lg_p_dt_vs_ene, p_energy.size(), &p_energy[0], &p_dt_vs_ene_325V[0], 
        colours.at(1), markers.at(1), 1.5, styles.at(2), "AP", "325V bias");
    addGraph(mg_p_dt_vs_ene, lg_p_dt_vs_ene, p_energy.size(), &p_energy[0], &p_dt_vs_ene_350V[0], 
        colours.at(0), markers.at(0), 1.5, styles.at(2), "AP", "350V bias");
    mg_p_dt_vs_ene->Draw("AP");
    mg_p_dt_vs_ene->GetXaxis()->SetTitle("beam energy [MeV]");
    mg_p_dt_vs_ene->GetYaxis()->SetTitle("#sigma_{t1-t2}/#sqrt{2} [ns]");
    mg_p_dt_vs_ene->GetYaxis()->SetTitleOffset(1.4);
    mg_p_dt_vs_ene->GetXaxis()->SetLimits(0, 300);
    mg_p_dt_vs_ene->SetMinimum(0.04);
    mg_p_dt_vs_ene->SetMaximum(0.08);
    lg_p_dt_vs_ene->Draw();
    canv_p_dt_vs_ene->SaveAs(out_file.c_str());
    
    out_file = res_path + "c_dt_vs_ene.pdf";
    auto canv_c_dt_vs_ene = std::make_shared<TCanvas>("c_dt_vs_ene", "c_dt_vs_ene");
    auto mg_c_dt_vs_ene = std::make_shared<TMultiGraph>();
    auto lg_c_dt_vs_ene = std::make_shared<TLegend>(0.65, 0.78, 0.87, 0.87);
    addGraph(mg_c_dt_vs_ene, lg_c_dt_vs_ene, c_energy.size(), &c_energy[0], &c_dt_vs_ene_200V[0], 
        colours.at(1), markers.at(1), 1.5, styles.at(2), "AP", "200V bias");
    addGraph(mg_c_dt_vs_ene, lg_c_dt_vs_ene, c_energy.size(), &c_energy[0], &c_dt_vs_ene_250V[0], 
        colours.at(0), markers.at(0), 1.5, styles.at(2), "AP", "250V bias");
    mg_c_dt_vs_ene->Draw("AP");
    mg_c_dt_vs_ene->GetXaxis()->SetTitle("beam energy [MeV/n]");
    mg_c_dt_vs_ene->GetYaxis()->SetTitle("#sigma_{t1-t2}/#sqrt{2} [ns]");
    mg_c_dt_vs_ene->GetYaxis()->SetTitleOffset(1.4);
    mg_c_dt_vs_ene->GetXaxis()->SetLimits(100, 450);
    mg_c_dt_vs_ene->SetMinimum(0.02);
    mg_c_dt_vs_ene->SetMaximum(0.06);
    lg_c_dt_vs_ene->Draw();
    canv_c_dt_vs_ene->SaveAs(out_file.c_str());
    
    out_file = res_path + "p_dt_vs_bias.pdf";
    auto canv_p_dt_vs_bias = std::make_shared<TCanvas>("p_dt_vs_bias", "p_dt_vs_bias");
    auto mg_p_dt_vs_bias = std::make_shared<TMultiGraph>();
    auto lg_p_dt_vs_bias = std::make_shared<TLegend>(0.65, 0.645, 0.87, 0.87);
    addGraph(mg_p_dt_vs_bias, lg_p_dt_vs_bias, p_bias_83.size(), &p_bias_83[0], &p_dt45_83[0], 
        colours.at(5), markers.at(5), 1.5, styles.at(2), "AP", "83 MeV");
    addGraph(mg_p_dt_vs_bias, lg_p_dt_vs_bias, p_bias_100.size(), &p_bias_100[0], &p_dt45_100[0], 
        colours.at(4), markers.at(4), 1.5, styles.at(2), "AP", "100 MeV");
    addGraph(mg_p_dt_vs_bias, lg_p_dt_vs_bias, p_bias_145.size(), &p_bias_145[0], &p_dt45_145[0], 
        colours.at(3), markers.at(3), 1.5, styles.at(2), "AP", "145 MeV");
    addGraph(mg_p_dt_vs_bias, lg_p_dt_vs_bias, p_bias_194.size(), &p_bias_194[0], &p_dt45_194[0], 
        colours.at(2), markers.at(2), 1.5, styles.at(2), "AP", "194 MeV");
    addGraph(mg_p_dt_vs_bias, lg_p_dt_vs_bias, p_bias_250.size(), &p_bias_250[0], &p_dt45_250[0], 
        colours.at(1), markers.at(1), 1.5, styles.at(2), "AP", "250 MeV");
    // addGraph(mg_p_dt_vs_bias, lg_p_dt_vs_bias, p_bias_800.size(), &p_bias_800[0], &p_dt45_800[0], 
    //     colours.at(0), markers.at(0), 1.5, styles.at(2), "AP", "800 MeV");
    mg_p_dt_vs_bias->Draw("AP PLC PMC PFC");
    mg_p_dt_vs_bias->GetXaxis()->SetTitle("bias voltage [V]");
    mg_p_dt_vs_bias->GetYaxis()->SetTitle("#sigma_{t1-t2}/#sqrt{2} [ns]");
    mg_p_dt_vs_bias->GetYaxis()->SetTitleOffset(1.4);
    mg_p_dt_vs_bias->GetXaxis()->SetLimits(180, 380);
    mg_p_dt_vs_bias->SetMinimum(0.04);
    mg_p_dt_vs_bias->SetMaximum(0.08);
    lg_p_dt_vs_bias->Draw();
    canv_p_dt_vs_bias->SaveAs(out_file.c_str());

    out_file = res_path + "c_dt_vs_bias.pdf";
    auto canv_c_dt_vs_bias = std::make_shared<TCanvas>("c_dt_vs_bias", "c_dt_vs_bias");
    auto mg_c_dt_vs_bias = std::make_shared<TMultiGraph>();
    auto lg_c_dt_vs_bias = std::make_shared<TLegend>(0.65, 0.69, 0.87, 0.87);
    addGraph(mg_c_dt_vs_bias, lg_c_dt_vs_bias, c_bias_120.size(), &c_bias_120[0], &c_dt45_120[0], 
        colours.at(3), markers.at(3), 1.5, styles.at(2), "AP", "120 MeV/n");
    addGraph(mg_c_dt_vs_bias, lg_c_dt_vs_bias, c_bias_200.size(), &c_bias_200[0], &c_dt45_200[0], 
        colours.at(2), markers.at(2), 1.5, styles.at(2), "AP", "200 MeV/n");
    addGraph(mg_c_dt_vs_bias, lg_c_dt_vs_bias, c_bias_300.size(), &c_bias_300[0], &c_dt45_300[0], 
        colours.at(1), markers.at(1), 1.5, styles.at(2), "AP", "300 MeV/n");
    addGraph(mg_c_dt_vs_bias, lg_c_dt_vs_bias, c_bias_400.size(), &c_bias_400[0], &c_dt45_400[0], 
        colours.at(0), markers.at(0), 1.5, styles.at(2), "AP", "400 MeV/n");
    mg_c_dt_vs_bias->Draw("AP PLC PMC PFC");
    mg_c_dt_vs_bias->GetXaxis()->SetTitle("bias voltage [V]");
    mg_c_dt_vs_bias->GetYaxis()->SetTitle("#sigma_{t1-t2}/#sqrt{2} [ns]");
    mg_c_dt_vs_bias->GetYaxis()->SetTitleOffset(1.4);
    mg_c_dt_vs_bias->GetXaxis()->SetLimits(100, 380);
    mg_c_dt_vs_bias->SetMinimum(0.02);
    mg_c_dt_vs_bias->SetMaximum(0.06);
    lg_c_dt_vs_bias->Draw();
    canv_c_dt_vs_bias->SaveAs(out_file.c_str());
    
    out_file = res_path + "p_sn_vs_bias.pdf";
    auto canv_p_sn_vs_bias = std::make_shared<TCanvas>("p_sn_vs_bias", "p_sn_vs_bias");
    auto mg_p_sn_vs_bias = std::make_shared<TMultiGraph>();
    auto lg_p_sn_vs_bias = std::make_shared<TLegend>(0.23, 0.60, 0.37, 0.87);
    addGraph(mg_p_sn_vs_bias, lg_p_sn_vs_bias, p_bias_83.size(), &p_bias_83[0], &p_sn_83[0], 
        colours.at(1), markers.at(5), 1.2, styles.at(2), "AP", "83 MeV");
    addGraph(mg_p_sn_vs_bias, lg_p_sn_vs_bias, p_bias_100.size(), &p_bias_100[0], &p_sn_100[0], 
        colours.at(1), markers.at(4), 1.2, styles.at(2), "AP", "100 MeV");
    addGraph(mg_p_sn_vs_bias, lg_p_sn_vs_bias, p_bias_145.size(), &p_bias_145[0], &p_sn_145[0], 
        colours.at(1), markers.at(3), 1.2, styles.at(2), "AP", "145 MeV");
    addGraph(mg_p_sn_vs_bias, lg_p_sn_vs_bias, p_bias_194.size(), &p_bias_194[0], &p_sn_194[0], 
        colours.at(1), markers.at(2), 1.2, styles.at(2), "AP", "194 MeV");
    addGraph(mg_p_sn_vs_bias, lg_p_sn_vs_bias, p_bias_250.size(), &p_bias_250[0], &p_sn_250[0], 
        colours.at(1), markers.at(1), 1.2, styles.at(2), "AP", "250 MeV");
    // addGraph(mg_p_sn_vs_bias, lg_p_sn_vs_bias, p_bias_800.size(), &p_bias_800[0], &p_sn_800[0], 
    //     colours.at(1), markers.at(0), 1.2, styles.at(2), "AP", "800 MeV");
    mg_p_sn_vs_bias->Draw("AP PLC PMC PFC");
    mg_p_sn_vs_bias->GetXaxis()->SetTitle("bias voltage [V]");
    mg_p_sn_vs_bias->GetYaxis()->SetTitle("mean effective S/N");
    mg_p_sn_vs_bias->GetYaxis()->SetTitleOffset(1.2);
    mg_p_sn_vs_bias->GetXaxis()->SetLimits(180, 370);
    mg_p_sn_vs_bias->SetMinimum(0);
    mg_p_sn_vs_bias->SetMaximum(100);
    lg_p_sn_vs_bias->Draw();
    canv_p_sn_vs_bias->SaveAs(out_file.c_str());

    out_file = res_path + "p_trise_vs_bias.pdf";
    auto canv_p_trise_vs_bias = std::make_shared<TCanvas>("p_trise_vs_bias", "p_trise_vs_bias");
    auto mg_p_trise_vs_bias = std::make_shared<TMultiGraph>();
    auto lg_p_trise_vs_bias = std::make_shared<TLegend>(0.65, 0.60, 0.87, 0.87);
    // addErrGraph(mg_p_trise_vs_bias, lg_p_trise_vs_bias, p_bias_62.size(), &p_bias_62[0], &p_trise_62[0], 
    //     &zero_err[0], &p_trise_var_62[0], colours.at(1), markers.at(5), 1.2, styles.at(2), "AP", "62 MeV");
    addGraph(mg_p_trise_vs_bias, lg_p_trise_vs_bias, p_bias_83.size(), &p_bias_83[0], &p_trise_83[0], 
        colours.at(1), markers.at(5), 1.2, styles.at(2), "AP", "83 MeV");
    addGraph(mg_p_trise_vs_bias, lg_p_trise_vs_bias, p_bias_100.size(), &p_bias_100[0], &p_trise_100[0], 
        colours.at(1), markers.at(4), 1.2, styles.at(2), "AP", "100 MeV");
    addGraph(mg_p_trise_vs_bias, lg_p_trise_vs_bias, p_bias_145.size(), &p_bias_145[0], &p_trise_145[0], 
        colours.at(1), markers.at(3), 1.2, styles.at(2), "AP", "145 MeV");
    addGraph(mg_p_trise_vs_bias, lg_p_trise_vs_bias, p_bias_194.size(), &p_bias_194[0], &p_trise_194[0], 
        colours.at(1), markers.at(2), 1.2, styles.at(2), "AP", "194 MeV");
    addGraph(mg_p_trise_vs_bias, lg_p_trise_vs_bias, p_bias_250.size(), &p_bias_250[0], &p_trise_250[0], 
        colours.at(1), markers.at(1), 1.2, styles.at(2), "AP", "250 MeV");
    addGraph(mg_p_trise_vs_bias, lg_p_trise_vs_bias, p_bias_800.size(), &p_bias_800[0], &p_trise_800[0], 
        colours.at(1), markers.at(0), 1.2, styles.at(2), "AP", "800 MeV");
    mg_p_trise_vs_bias->Draw("AP PLC PMC PFC");
    mg_p_trise_vs_bias->GetXaxis()->SetTitle("bias voltage [V]");
    mg_p_trise_vs_bias->GetYaxis()->SetTitle("t_{rise} [ns]");
    mg_p_trise_vs_bias->GetYaxis()->SetTitleOffset(1.2);
    mg_p_trise_vs_bias->GetXaxis()->SetLimits(180, 370);
    mg_p_trise_vs_bias->SetMinimum(0.400);
    mg_p_trise_vs_bias->SetMaximum(0.700);
    lg_p_trise_vs_bias->Draw();
    canv_p_trise_vs_bias->SaveAs(out_file.c_str());
    
    out_file = res_path + "p_trise_var_vs_bias.pdf";
    auto canv_p_trise_var_vs_bias = std::make_shared<TCanvas>("p_trise_var_vs_bias", "p_trise_var_vs_bias");
    auto mg_p_trise_var_vs_bias = std::make_shared<TMultiGraph>();
    auto lg_p_trise_var_vs_bias = std::make_shared<TLegend>(0.23, 0.20, 0.37, 0.47);
    // addErrGraph(mg_p_trise_var_vs_bias, lg_p_trise_var_vs_bias, p_bias_62.size(), &p_bias_62[0], &p_trise_var_62[0], 
    //     &zero_err[0], &p_trise_var_var_62[0], colours.at(1), markers.at(5), 1.2, styles.at(2), "AP", "62 MeV");
    addGraph(mg_p_trise_var_vs_bias, lg_p_trise_var_vs_bias, p_bias_83.size(), &p_bias_83[0], &p_trise_var_83[0], 
        colours.at(1), markers.at(5), 1.2, styles.at(2), "AP", "83 MeV");
    addGraph(mg_p_trise_var_vs_bias, lg_p_trise_var_vs_bias, p_bias_100.size(), &p_bias_100[0], &p_trise_var_100[0], 
        colours.at(1), markers.at(4), 1.2, styles.at(2), "AP", "100 MeV");
    addGraph(mg_p_trise_var_vs_bias, lg_p_trise_var_vs_bias, p_bias_145.size(), &p_bias_145[0], &p_trise_var_145[0], 
        colours.at(1), markers.at(3), 1.2, styles.at(2), "AP", "145 MeV");
    addGraph(mg_p_trise_var_vs_bias, lg_p_trise_var_vs_bias, p_bias_194.size(), &p_bias_194[0], &p_trise_var_194[0], 
        colours.at(1), markers.at(2), 1.2, styles.at(2), "AP", "194 MeV");
    addGraph(mg_p_trise_var_vs_bias, lg_p_trise_var_vs_bias, p_bias_250.size(), &p_bias_250[0], &p_trise_var_250[0], 
        colours.at(1), markers.at(1), 1.2, styles.at(2), "AP", "250 MeV");
    addGraph(mg_p_trise_var_vs_bias, lg_p_trise_var_vs_bias, p_bias_800.size(), &p_bias_800[0], &p_trise_var_800[0], 
        colours.at(1), markers.at(0), 1.2, styles.at(2), "AP", "800 MeV");
    mg_p_trise_var_vs_bias->Draw("AP PLC PMC PFC");
    mg_p_trise_var_vs_bias->GetXaxis()->SetTitle("bias voltage [V]");
    mg_p_trise_var_vs_bias->GetYaxis()->SetTitle("#sigma_{trise} [ns]");
    mg_p_trise_var_vs_bias->GetYaxis()->SetTitleOffset(1.2);
    mg_p_trise_var_vs_bias->GetXaxis()->SetLimits(180, 370);
    mg_p_trise_var_vs_bias->SetMinimum(0.0);
    mg_p_trise_var_vs_bias->SetMaximum(0.08);
    lg_p_trise_var_vs_bias->Draw();
    canv_p_trise_var_vs_bias->SaveAs(out_file.c_str());

    out_file = res_path + "c_sn_vs_bias.pdf";
    auto canv_c_sn_vs_bias = std::make_shared<TCanvas>("c_sn_vs_bias", "c_sn_vs_bias");
    auto mg_c_sn_vs_bias = std::make_shared<TMultiGraph>();
    auto lg_c_sn_vs_bias = std::make_shared<TLegend>(0.23, 0.69, 0.37, 0.87);
    addGraph(mg_c_sn_vs_bias, lg_c_sn_vs_bias, c_bias_120.size(), &c_bias_120[0], &c_sn_120[0], 
        colours.at(3), markers.at(3), 1.2, styles.at(2), "AP", "120 MeV/n");
    addGraph(mg_c_sn_vs_bias, lg_c_sn_vs_bias, c_bias_200.size(), &c_bias_200[0], &c_sn_200[0], 
        colours.at(2), markers.at(2), 1.2, styles.at(2), "AP", "200 MeV/n");
    addGraph(mg_c_sn_vs_bias, lg_c_sn_vs_bias, c_bias_300.size(), &c_bias_300[0], &c_sn_300[0], 
        colours.at(1), markers.at(1), 1.2, styles.at(2), "AP", "300 MeV/n");
    addGraph(mg_c_sn_vs_bias, lg_c_sn_vs_bias, c_bias_400.size(), &c_bias_400[0], &c_sn_400[0], 
        colours.at(0), markers.at(0), 1.2, styles.at(2), "AP", "400 MeV/n");
    mg_c_sn_vs_bias->Draw("AP PLC PMC PFC");
    mg_c_sn_vs_bias->GetXaxis()->SetTitle("bias voltage [V]");
    mg_c_sn_vs_bias->GetYaxis()->SetTitle("mean effective S/N");
    mg_c_sn_vs_bias->GetYaxis()->SetTitleOffset(1.4);
    mg_c_sn_vs_bias->GetXaxis()->SetLimits(100, 380);
    mg_c_sn_vs_bias->SetMinimum(0);
    mg_c_sn_vs_bias->SetMaximum(200);
    lg_c_sn_vs_bias->Draw();
    canv_c_sn_vs_bias->SaveAs(out_file.c_str());
    
    out_file = res_path + "c_trise_vs_bias.pdf";
    auto canv_c_trise_vs_bias = std::make_shared<TCanvas>("c_trise_vs_bias", "c_trise_vs_bias");
    auto mg_c_trise_vs_bias = std::make_shared<TMultiGraph>();
    auto lg_c_trise_vs_bias = std::make_shared<TLegend>(0.65, 0.69, 0.87, 0.87);
    addErrGraph(mg_c_trise_vs_bias, lg_c_trise_vs_bias, c_bias_120.size(), &c_bias_120[0], &c_trise_120[0], 
        &zero_err[0], &c_trise_var_120[0], colours.at(3), markers.at(3), 1.2, styles.at(2), "AP", "120 MeV/n");
    addErrGraph(mg_c_trise_vs_bias, lg_c_trise_vs_bias, c_bias_200.size(), &c_bias_200[0], &c_trise_200[0], 
        &zero_err[0], &c_trise_var_200[0],colours.at(2), markers.at(2), 1.2, styles.at(2), "AP", "200 MeV/n");
    addErrGraph(mg_c_trise_vs_bias, lg_c_trise_vs_bias, c_bias_300.size(), &c_bias_300[0], &c_trise_300[0], 
        &zero_err[0], &c_trise_var_300[0],colours.at(1), markers.at(1), 1.2, styles.at(2), "AP", "300 MeV/n");
    addErrGraph(mg_c_trise_vs_bias, lg_c_trise_vs_bias, c_bias_400.size(), &c_bias_400[0], &c_trise_400[0], 
        &zero_err[0], &c_trise_var_400[0],colours.at(0), markers.at(0), 1.2, styles.at(2), "AP", "400 MeV/n");
    mg_c_trise_vs_bias->Draw("AP PLC PMC PFC");
    mg_c_trise_vs_bias->GetXaxis()->SetTitle("bias voltage [V]");
    mg_c_trise_vs_bias->GetYaxis()->SetTitle("rise time [ns]");
    mg_c_trise_vs_bias->GetYaxis()->SetTitleOffset(1.4);
    mg_c_trise_vs_bias->GetXaxis()->SetLimits(100, 380);
    mg_c_trise_vs_bias->SetMinimum(0.400);
    mg_c_trise_vs_bias->SetMaximum(0.700);
    lg_c_trise_vs_bias->Draw();
    canv_c_trise_vs_bias->SaveAs(out_file.c_str());

    out_file = res_path + "all_dt_vs_ene.pdf";
    auto canv_all_dt_vs_ene = std::make_shared<TCanvas>("all_dt_vs_ene", "all_dt_vs_ene");
    auto mg_all_dt_vs_ene = std::make_shared<TMultiGraph>();
    auto lg_all_dt_vs_ene = std::make_shared<TLegend>(0.65, 0.78, 0.87, 0.87);
    addGraph(mg_all_dt_vs_ene, lg_all_dt_vs_ene, p_energy.size(), &p_energy[0], &p_dt_vs_ene_350V[0], 
        colours.at(1), markers.at(1), 1.2, styles.at(2), "AP", "protons");
    addGraph(mg_all_dt_vs_ene, lg_all_dt_vs_ene, c_energy.size(), &c_energy[0], &c_dt_vs_ene_250V[0], 
        colours.at(0), markers.at(0), 1.2, styles.at(2), "AP", "carbons");
    mg_all_dt_vs_ene->Draw("AP");
    mg_all_dt_vs_ene->GetXaxis()->SetTitle("beam energy [MeV/n]");
    mg_all_dt_vs_ene->GetYaxis()->SetTitle("#sigma_{t1-t2}/#sqrt{2} [ns]");
    mg_all_dt_vs_ene->GetYaxis()->SetTitleOffset(1.4);
    mg_all_dt_vs_ene->GetXaxis()->SetLimits(0, 850);
    mg_all_dt_vs_ene->SetMinimum(0.02);
    mg_all_dt_vs_ene->SetMaximum(0.08);
    lg_all_dt_vs_ene->Draw();
    canv_all_dt_vs_ene->SaveAs(out_file.c_str());

    out_file = res_path + "p_tof_vs_ene.pdf";
    auto canv_p_tof_vs_ene = std::make_shared<TCanvas>("p_tof_vs_ene", "p_tof_vs_ene");
    auto mg_p_tof_vs_ene = std::make_shared<TMultiGraph>();
    auto lg_p_tof_vs_ene = std::make_shared<TLegend>(0.65, 0.78, 0.87, 0.87);
    for (auto &v : p_tof_vs_ene_350V){ v -= p_tof_vs_ene_350V.at(6) + 0.0991; v = abs(v); }
    addGraph(mg_p_tof_vs_ene, lg_p_tof_vs_ene, p_energy.size(), &p_energy[0], &p_tof_vs_ene_350V[0], 
        colours.at(1), markers.at(1), 1.2, styles.at(2), "AP", "measured");
    addGraph(mg_p_tof_vs_ene, lg_p_tof_vs_ene, p_energy.size(), &p_energy[0], &p_time_vs_ene[0],      
        colours.at(0), markers.at(0), 1.2, styles.at(2), "AP", "theory");
    mg_p_tof_vs_ene->Draw("AP");
    mg_p_tof_vs_ene->GetXaxis()->SetTitle("beam energy [MeV/n]");
    mg_p_tof_vs_ene->GetYaxis()->SetTitle("tof [ns]");
    mg_p_tof_vs_ene->GetYaxis()->SetTitleOffset(1.2);
    mg_p_tof_vs_ene->GetXaxis()->SetLimits(0, 850);
    mg_p_tof_vs_ene->SetMinimum(0.0);
    mg_p_tof_vs_ene->SetMaximum(0.4);
    lg_p_tof_vs_ene->Draw();
    canv_p_tof_vs_ene->SaveAs(out_file.c_str());

    out_file = res_path + "c_tof_vs_ene.pdf";
    auto canv_c_tof_vs_ene = std::make_shared<TCanvas>("c_tof_vs_ene", "c_tof_vs_ene");
    auto mg_c_tof_vs_ene = std::make_shared<TMultiGraph>();
    auto lg_c_tof_vs_ene = std::make_shared<TLegend>(0.65, 0.78, 0.87, 0.87);
    for (auto &v : c_tof_vs_ene_250V){ v -= c_tof_vs_ene_250V.at(3) + 0.1167; v = abs(v); }
    addGraph(mg_c_tof_vs_ene, lg_c_tof_vs_ene, c_energy.size(), &c_energy[0], &c_tof_vs_ene_250V[0], 
        colours.at(1), markers.at(1), 1.2, styles.at(2), "AP", "measured");
    addGraph(mg_c_tof_vs_ene, lg_c_tof_vs_ene, c_energy.size(), &c_energy[0], &c_time_vs_ene[0],      
        colours.at(0), markers.at(0), 1.2, styles.at(2), "AP", "theory");
    mg_c_tof_vs_ene->Draw("AP");
    mg_c_tof_vs_ene->GetXaxis()->SetTitle("beam energy [MeV/n]");
    mg_c_tof_vs_ene->GetYaxis()->SetTitle("tof [ns]");
    mg_c_tof_vs_ene->GetYaxis()->SetTitleOffset(1.2);
    mg_c_tof_vs_ene->GetXaxis()->SetLimits(0, 850);
    mg_c_tof_vs_ene->SetMinimum(0.0);
    mg_c_tof_vs_ene->SetMaximum(0.4);
    lg_c_tof_vs_ene->Draw();
    canv_c_tof_vs_ene->SaveAs(out_file.c_str());
    
    out_file = res_path + "theory_charge_vs_ene.pdf";
    auto canv_all_charge_vs_ene = std::make_shared<TCanvas>("all_charge_vs_ene", "all_charge_vs_ene");
    auto mg_all_charge_vs_ene = std::make_shared<TMultiGraph>();
    auto lg_all_charge_vs_ene = std::make_shared<TLegend>(0.65, 0.78, 0.87, 0.87);
    for (auto &v : p_dep_mpv_vs_ene){ v -= 0.22278; }
    for (auto &v : h_dep_mpv_vs_ene){ v -= 0.22278; }
    canv_all_charge_vs_ene->SetLogy();
    addGraph(mg_all_charge_vs_ene, lg_all_charge_vs_ene, p_energy.size(), &p_energy[0], &p_dep_mpv_vs_ene[0], 
        colours.at(2), markers.at(2), 1.2, styles.at(2), "AP", "protons");
    addGraph(mg_all_charge_vs_ene, lg_all_charge_vs_ene, c_energy.size(), &c_energy[0], &h_dep_mpv_vs_ene[0],      
        colours.at(1), markers.at(1), 1.2, styles.at(2), "AP", "helium");
    addGraph(mg_all_charge_vs_ene, lg_all_charge_vs_ene, c_energy.size(), &c_energy[0], &c_dep_mpv_vs_ene[0],      
        colours.at(0), markers.at(0), 1.2, styles.at(2), "AP", "carbon");
    mg_all_charge_vs_ene->Draw("APL");
    mg_all_charge_vs_ene->GetXaxis()->SetTitle("beam energy (MeV/n)");
    mg_all_charge_vs_ene->GetYaxis()->SetTitle("MPV in 50 um silicon [ke-]");
    mg_all_charge_vs_ene->GetYaxis()->SetTitleOffset(1.2);
    mg_all_charge_vs_ene->GetXaxis()->SetLimits(0, 850);
    // mg_all_charge_vs_ene->SetMinimum(0.00);
    // mg_all_charge_vs_ene->SetMaximum(0.85);
    lg_all_charge_vs_ene->Draw();
    // gPad->SetLogy();
    canv_all_charge_vs_ene->SaveAs(out_file.c_str());

    out_file = res_path + "lab_saturation.pdf";
    auto canv_lab_saturation = std::make_shared<TCanvas>("lab_saturation", "lab_saturation");
    auto mg_lab_saturation = std::make_shared<TMultiGraph>();
    auto lg_lab_saturation = std::make_shared<TLegend>(0.20, 0.78, 0.44, 0.87);
    addGraph(mg_lab_saturation, lg_lab_saturation, l_intensity.size(), &l_intensity[0], &l_charge_1st[0],
        colours.at(1), markers.at(1), 1.2, styles.at(2), "AP", "1st amplification stage (x100)");
    addGraph(mg_lab_saturation, lg_lab_saturation, l_intensity.size(), &l_intensity[0], &l_charge_2nd[0],
        colours.at(0), markers.at(0), 1.2, styles.at(2), "AP", "2nd amplification stage");
    mg_lab_saturation->Draw("APC");
    mg_lab_saturation->GetXaxis()->SetTitle("laser intensity (%)");
    mg_lab_saturation->GetYaxis()->SetTitle("signal charge [a.U]");
    mg_lab_saturation->GetYaxis()->SetTitleOffset(1.2);
    mg_lab_saturation->GetXaxis()->SetLimits(-5, 105);
    mg_lab_saturation->SetMinimum(0.00);
    mg_lab_saturation->SetMaximum(140);
    lg_lab_saturation->Draw();
    canv_lab_saturation->SaveAs(out_file.c_str());

    out_file = res_path + "lab_gain.pdf";
    auto canv_lab_gain = std::make_shared<TCanvas>("lab_gain", "lab_gain");
    auto mg_lab_gain = std::make_shared<TMultiGraph>();
    auto lg_lab_gain = std::make_shared<TLegend>(0.20, 0.78, 0.44, 0.87);
    addGraph(mg_lab_gain, lg_lab_gain, l_volts.size(), &l_volts[0], &l_charge[0],
        colours.at(0), markers.at(0), 1.2, styles.at(2), "AP", "1st amplification stage (x100)");
    mg_lab_gain->Draw("APC");
    mg_lab_gain->GetXaxis()->SetTitle("bias voltage [V]");
    mg_lab_gain->GetYaxis()->SetTitle("gain");
    mg_lab_gain->GetYaxis()->SetTitleOffset(1.2);
    mg_lab_gain->GetXaxis()->SetLimits(0, 430);
    mg_lab_gain->SetMinimum(0.00);
    mg_lab_gain->SetMaximum(28);
    // lg_lab_gain->Draw();
    canv_lab_gain->SaveAs(out_file.c_str());

    return 0;
}




std::shared_ptr<runResults> analyse_parallel(std::string path, std::string folder, std::string fn, std::string species) {

    styleCommon();
    styleMap();
    styleGraph();


    // Setup
    // ---------------------------------------

    float thr = 0.005;

    float nbins_time = 100;
    float vmin_time = -1;
    float vmax_time = +1;

    float nbins_amp = 100;
    float vmin_amp = 0;
    float vmax_amp = 0.1;

    float nbins_charge = 100;
    float vmin_charge = 0;
    float vmax_charge = 2.5;

    float nbins_noise = 100;
    float vmin_noise = 0;
    float vmax_noise = 0.001;

    if (species == "carbons"){
        vmax_charge = 15;
        vmax_amp = 0.4;
        thr = 0.01;
    }
    
    if (species == "trigger"){
        vmax_charge = 100;
        vmax_amp = 2;
        thr = 0.005;
    }
    
    if (species == "laser"){
        vmax_charge = 5;
        vmax_amp = 0.1;
        thr = 0.003;
    }



    // Preperations
    // ---------------------------------------

    int event, ret, nevents;
    bool info = false;
    std::string out_file, dat_file_01, dat_file_02;
    std::vector<float> dat_test, xpeaks, ypeaks;
    std::vector<float> dat_01, peaks_01, dat_02, peaks_02;

    dat_file_01 = path + folder + "/" + fn + "_Ch3.dat";
    dat_file_02 = path + folder + "/" + fn + "_Ch4.dat";
    ret = read_array(dat_file_01, ' ', dat_01, false);
    std::cout << "-- Reading data file: " << dat_file_01 << ". " << dat_01.size() << " entries." << std::endl;
    ret = read_array(dat_file_02, ' ', dat_02, false);
    std::cout << "-- Reading data file: " << dat_file_02 << ". " << dat_02.size() << " entries." <<  std::endl;

    // folder = "setup6_test";
    nevents = static_cast<int> (dat_01.size()/1000.);
    


    // Analysis
    // --------------------------------------
    
    auto vals_01 = std::make_shared<peakValues> ();
    auto vals_02 = std::make_shared<peakValues> ();

    std::cout << "-- Starting analysis " << std::endl;

	auto h1_noise = std::make_shared<TH1F>("", "", nbins_noise, vmin_noise, vmax_noise);
	auto h1_freq = std::make_shared<TH1F>("", "", 500, 0, 5000);
    auto h1_amp = std::make_shared<TH1F>("", "", nbins_amp, vmin_amp, vmax_amp);
    auto h1_charge = std::make_shared<TH1F>("", "", nbins_charge, vmin_charge, vmax_charge);
    auto h1_trise = std::make_shared<TH1F>("", "", 100, 0, 1);
    auto h2_noise = std::make_shared<TH1F>("", "", nbins_noise, vmin_noise, vmax_noise);
	auto h2_freq = std::make_shared<TH1F>("", "", 500, 0, 5000);
    auto h2_amp = std::make_shared<TH1F>("", "", nbins_amp, vmin_amp, vmax_amp);
    auto h2_charge = std::make_shared<TH1F>("", "", nbins_charge, vmin_charge, vmax_charge);
    auto h2_trise = std::make_shared<TH1F>("", "", 100, 0, 1);

    auto h_dtgaus = std::make_shared<TH1F>("", "", nbins_time, vmin_time, vmax_time);
    auto h_dt15 = std::make_shared<TH1F>("", "", nbins_time, vmin_time, vmax_time);
    auto h_dt30 = std::make_shared<TH1F>("", "", nbins_time, vmin_time, vmax_time);
    auto h_dt45 = std::make_shared<TH1F>("", "", nbins_time, vmin_time, vmax_time);
    auto h_dt60 = std::make_shared<TH1F>("", "", nbins_time, vmin_time, vmax_time);
    auto h_dt15n = std::make_shared<TH1F>("", "", nbins_time, vmin_time, vmax_time);
    auto h_dt30n = std::make_shared<TH1F>("", "", nbins_time, vmin_time, vmax_time);
    auto h_dt45n = std::make_shared<TH1F>("", "", nbins_time, vmin_time, vmax_time);
    auto h_dt60n = std::make_shared<TH1F>("", "", nbins_time, vmin_time, vmax_time);

    auto m_sn_vs_dt30 = std::make_shared<TH2F>("", "", 100, 0, 100, nbins_time, vmin_time, vmax_time);
    auto m_sn_vs_dt45 = std::make_shared<TH2F>("", "", 100, 0, 250, nbins_time, vmin_time, vmax_time);
    auto m_sn_vs_dt60 = std::make_shared<TH2F>("", "", 100, 0, 250, nbins_time, vmin_time, vmax_time);
    
    auto m_noise_vs_dt30 = std::make_shared<TH2F>("", "", nbins_noise, vmin_noise, vmax_noise, nbins_time, vmin_time, vmax_time);
    auto m_trise_vs_dt30 = std::make_shared<TH2F>("", "", 100, 0.8, 1.2, 160, -0.4, 0.4);

    // peak search
    int cnt_coinc = 0;
    int cnt_excpt = 0;
    float sn_1, sn_2, sn_eff;
    // for (int i = 0; i < nevents; i++){
    for (int i = 0; i < 100; i++){
        try {
            auto frame_01 = std::vector<float>(dat_01.begin() + i*1000, dat_01.begin() + i*1000 + 1000);
            auto frame_02 = std::vector<float>(dat_02.begin() + i*1000, dat_02.begin() + i*1000 + 1000);

            // npeaks1 = findAllPeaks(frame_01, xpeaks, ypeaks1, "pos", 0.15);
            // npeaks2 = findAllPeaks(frame_01, xpeaks, ypeaks2, "pos", 0.15);

            auto frame_01_odd = split_vector(frame_01);
            auto frame_02_odd = split_vector(frame_02);

            if (i%1000 == 0){
                out_file = res_path + folder + "/" + "wf_single_ch3_" + std::to_string(i) + "_" + fn + ".pdf";
                plotWaveform(frame_01, 0, out_file);
                out_file = res_path + folder + "/" + "wf_single_ch4_" + std::to_string(i) + "_" + fn + ".pdf";
                plotWaveform(frame_02, 0, out_file);
            }

            // vals_01 = analysePeak(frame_01, "pos", 100, 400);
            // vals_02 = analysePeak(frame_02, "pos", 100, 400);
            // vals_01 = analysePeak(frame_01, "pos", 0, 200);
            // vals_02 = analysePeak(frame_02, "pos", 0, 200);
            vals_01 = analysePeak(frame_01, "pos", 0, 700);
            vals_02 = analysePeak(frame_02, "pos", 0, 700);

            if ((vals_01->amp > thr) && (vals_02->amp > thr)) {

                sn_1 = vals_01->amp / vals_01->noise;
                sn_2 = vals_02->amp / vals_02->noise;
                sn_eff = (sn_1*sn_2) / sqrt(pow(sn_1, 2) + pow(sn_1, 2));

                // fill histogrammes
                h1_noise->Fill(vals_01->noise);
                h1_amp->Fill(vals_01->amp);
                h1_charge->Fill(vals_01->charge);
                h1_trise->Fill(vals_01->trise);
                // h1_freq->Fill(peaks_01.at(i) * 0.04 - tpeak);

                h2_noise->Fill(vals_02->noise);
                h2_amp->Fill(vals_02->amp);
                h2_charge->Fill(vals_02->charge);
                h2_trise->Fill(vals_02->trise);
                // h2_freq->Fill(peaks_02.at(i) * 0.04 - tpeak);

                h_dtgaus->Fill(vals_01->tpeak - vals_02->tpeak);
                h_dt15->Fill(vals_01->tlinear15 - vals_02->tlinear15);
                h_dt30->Fill(vals_01->tlinear30 - vals_02->tlinear30);
                h_dt45->Fill(vals_01->tlinear45 - vals_02->tlinear45);
                h_dt60->Fill(vals_01->tlinear60 - vals_02->tlinear60);
                h_dt15n->Fill(vals_01->tnorm15 - vals_02->tnorm15);
                h_dt30n->Fill(vals_01->tnorm30 - vals_02->tnorm30);
                h_dt45n->Fill(vals_01->tnorm45 - vals_02->tnorm45);
                h_dt60n->Fill(vals_01->tnorm60 - vals_02->tnorm60);

                m_sn_vs_dt30->Fill(sn_eff, vals_01->tlinear30 - vals_02->tlinear30);
                m_sn_vs_dt45->Fill(sn_eff, vals_01->tlinear45 - vals_02->tlinear45);
                m_sn_vs_dt60->Fill(sn_eff, vals_01->tlinear60 - vals_02->tlinear60);
                
                m_noise_vs_dt30->Fill(vals_01->noise+vals_02->noise, vals_01->tlinear30 - vals_02->tlinear30);
                m_trise_vs_dt30->Fill(vals_01->trise+vals_02->trise, vals_01->tlinear30 - vals_02->tlinear30);

                cnt_coinc++;
            }
        }
        catch (const std::out_of_range& e) {
            cnt_excpt++;
            cout << "An exception occurred." << '\n';
        }
    }

    // waveforms
    std::vector<float> x(5000);
    std::vector<float> errx(5000, 0);
    std::vector<float> erry(5000, 0);
    std::iota (std::begin(x), std::end(x), 0);
    for (int i = 0; i < 100; i++){
        auto peak_01 = std::vector<float>(dat_01.begin() + i*5000, dat_01.begin() + i*5000 + 5000);
        auto peak_02 = std::vector<float>(dat_02.begin() + i*5000, dat_02.begin() + i*5000 + 5000);
    }

    // fits
    auto res = std::shared_ptr<runResults> ();
    auto resf = std::shared_ptr<fitResult> ();
    resf = fitGauss(h_dtgaus, 0.1, -1, +1);
    resf = fitGauss(h_dt15, 0.1, -1, +1);
    resf = fitGauss(h_dt30, 0.1, -1, +1);
    resf = fitGauss(h_dt45, 0.1, -1, +1);
    resf = fitGauss(h_dt60, 0.1, -1, +1);
    resf = fitGauss(h_dt15n, 0.1, -1, +1);
    resf = fitGauss(h_dt30n, 0.1, -1, +1);
    resf = fitGauss(h_dt45n, 0.1, -1, +1);
    resf = fitGauss(h_dt60n, 0.1, -1, +1);

    resf = fitLandau(h1_charge, 0.0, 0, 40);
    resf = fitLandau(h2_charge, 0.0, 0, 40);
    resf = fitLandau(h1_amp, 0.0, 0, 0.5);
    resf = fitLandau(h2_amp, 0.0, 0, 0.5);

    auto gr_sn_vs_dt30 = std::make_shared<TGraphErrors> ();
    gr_sn_vs_dt30 = getProjectionFrom2DAlongY(m_sn_vs_dt30, 100, 0, 100, 50);
    auto gr_sn_vs_dt45 = std::make_shared<TGraphErrors> ();
    gr_sn_vs_dt45 = getProjectionFrom2DAlongY(m_sn_vs_dt45, 100, 0, 250, 50);
    auto gr_sn_vs_dt60 = std::make_shared<TGraphErrors> ();
    gr_sn_vs_dt60 = getProjectionFrom2DAlongY(m_sn_vs_dt60, 100, 0, 250, 50);

    

    // Print
    // ---------------------------------------

    styleGraph();
    out_file = res_path + folder + "/" + "h1_noise_" + fn + ".pdf";
    printHist(h1_noise, out_file, "", "noise (V)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h1_amp_" + fn + ".pdf";
    printHist(h1_amp, out_file, "", "waveform amplitude (V)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h1_charge_" + fn + ".pdf";
    printHist(h1_charge, out_file, "", "waveform integral (V*ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h1_trise_" + fn + ".pdf";
    printHist(h1_trise, out_file, "", "rise time (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h1_freq_" + fn + ".pdf";
    printHist(h1_freq, out_file, "", "time difference between hits (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h2_noise_" + fn + ".pdf";
    printHist(h2_noise, out_file, "", "noise (V)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h2_amp_" + fn + ".pdf";
    printHist(h2_amp, out_file, "", "waveform amplitude (V)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h2_charge_" + fn + ".pdf";
    printHist(h2_charge, out_file, "", "waveform integral (V*ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h2_trise_" + fn + ".pdf";
    printHist(h2_trise, out_file, "", "rise time (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h2_freq_" + fn + ".pdf";
    printHist(h2_freq, out_file, "", "time difference between hits (ns)", "# events",-1.0, -1.0, 1.2);

    out_file = res_path + folder + "/" + "h_dtgaus_" + fn + ".pdf";
    printHist(h_dtgaus, out_file, "", "dtgaus (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_dt15_" + fn + ".pdf";
    printHist(h_dt15, out_file, "", "dt15 (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_dt30_" + fn + ".pdf";
    printHist(h_dt30, out_file, "", "dt30 (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_dt45_" + fn + ".pdf";
    printHist(h_dt45, out_file, "", "dt45 (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_dt60_" + fn + ".pdf";
    printHist(h_dt60, out_file, "", "dt60 (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_dt15n_" + fn + ".pdf";
    printHist(h_dt15n, out_file, "", "dt15 (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_dt30n_" + fn + ".pdf";
    printHist(h_dt30n, out_file, "", "dt30 (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_dt45n_" + fn + ".pdf";
    printHist(h_dt45n, out_file, "", "dt45 (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_dt60n_" + fn + ".pdf";
    printHist(h_dt60n, out_file, "", "dt60 (ns)", "# events",-1.0, -1.0, 1.2);

    out_file = res_path + folder + "/" + "gr_sn_vs_dt30_" + fn + ".pdf";
    printGraph(gr_sn_vs_dt30, out_file, "", "sn_eff", "dt30 rms (ns)",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "gr_sn_vs_dt45_" + fn + ".pdf";
    printGraph(gr_sn_vs_dt45, out_file, "", "sn_eff", "dt45 rms (ns)",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "gr_sn_vs_dt60_" + fn + ".pdf";
    printGraph(gr_sn_vs_dt60, out_file, "", "sn_eff", "dt60 (ns)",-1.0, -1.0, 1.2);

    out_file = res_path + folder + "/" + "m_sn_vs_dt30_" + fn + ".pdf";
    printMap(m_sn_vs_dt30, out_file, "", "sn_eff", "dt30 (ns)", "# events", -1.0, -1.0, 1.2, 1.4);
    out_file = res_path + folder + "/" + "m_sn_vs_dt45_" + fn + ".pdf";
    printMap(m_sn_vs_dt45, out_file, "", "sn_eff", "dt45 (ns)", "# events", -1.0, -1.0, 1.2, 1.4);
    out_file = res_path + folder + "/" + "m_sn_vs_dt60_" + fn + ".pdf";
    printMap(m_sn_vs_dt60, out_file, "", "sn_eff", "dt60 (ns)", "# events", -1.0, -1.0, 1.2, 1.4);
    
    out_file = res_path + folder + "/" + "m_noise_vs_dt30_" + fn + ".pdf";
    printMap(m_noise_vs_dt30, out_file, "", "noise [V]", "dt30 (ns)", "# events", -1.0, -1.0, 1.2, 1.4);
    out_file = res_path + folder + "/" + "m_trise_vs_dt30_" + fn + ".pdf";
    printMap(m_trise_vs_dt30, out_file, "", "trise [ns]", "dt30 (ns)", "# events", -1.0, -1.0, 1.2, 1.4);



    std::cout << "-- Analysed run " << folder << " Found: "
              << cnt_coinc << " coincidencent signals."
              << cnt_excpt << " exceptions."
              << std::endl;
    std::cout << std::endl << std::endl << std::endl;


    // Write Results
    // ---------------------------------------

    std::vector<std::vector<float>> out;
    // out.push_back({res->amp1_mean, res->amp1_rms, res->amp1_err, res->amp2_mean, res->amp2_rms, res->amp2_err});
    // out.push_back({res->charge1_mean, res->charge1_rms, res->charge1_err,
    //     res->charge2_mean, res->charge2_rms, res->charge2_err});
    // out.push_back({res->noise1_mean, res->noise1_rms, res->noise1_err,
    //     res->noise2_mean, res->noise2_rms, res->noise2_err});
    // out.push_back({res->trise1_mean, res->trise1_rms, res->trise1_err,
    //     res->trise2_mean, res->trise2_rms, res->trise2_err});
    // out.push_back({res->dt10_mean, res->dt10_rms, res->dt10_err,
    //     res->dt15_mean, res->dt15_rms, res->dt15_err});
    // out.push_back({res->dt20_mean, res->dt20_rms, res->dt20_err,
    //     res->dt25_mean, res->dt25_rms, res->dt25_err});
    // out.push_back({res->dt30_mean, res->dt30_rms, res->dt30_err,
    //     res->dt35_mean, res->dt35_rms, res->dt35_err});
    // out.push_back({res->dt45_mean, res->dt45_rms, res->dt45_err,
    //     res->dt60_mean, res->dt60_rms, res->dt60_err});

    std::string hd = "# ch1 mean | ch1 rms | ch1 err | ch2 mean | ch1 rms | ch1 err\n";
    hd += "# amp | charge | noise | trise | dt10 | dt15 |dt20 | dt25 | dt30 | dt35 | dt45 | dt60";

    out_file = res_path + folder + "/" + "results.txt";
    // write_file(out_file, '\t', out, hd, info);

    // delete h1_noise; delete h1_freq; delete h1_amp; delete h1_charge; delete h1_trise; 
    // delete h2_noise; delete h2_freq; delete h2_amp; delete h2_charge; delete h2_trise;
    // delete h_dtgaus; delete h_dt30; delete h_dt45; delete h_dt60;
    // delete vals_01; delete vals_02;

    return res;
}





std::shared_ptr<runResults> analyse_series(std::string path, std::string folder, std::string fn, std::string species) {

    styleCommon();
    styleMap();
    styleGraph();


    // Setup
    // ---------------------------------------

    float thr = 0.005;

    float nbins_time = 100;
    float vmin_time = -0.5;
    float vmax_time = +0.5;

    float nbins_amp = 200;
    float vmin_amp = 0;
    float vmax_amp = 0.1;

    float nbins_charge = 200;
    float vmin_charge = 0;
    float vmax_charge = 2.5;

    float nbins_noise = 100;
    float vmin_noise = 0;
    float vmax_noise = 0.003;

    if (species == "carbons"){
        vmax_charge = 15;
        vmax_amp = 0.4;
        thr = 0.04;
    }
    
    if (species == "laser"){
        vmax_charge = 15;
        vmax_amp = 0.4;
        thr = 0.001;
    }
    
    if (species == "trigger"){
        vmax_charge = 100;
        vmax_amp = 2;
        thr = 0.005;
    }


    // Preperations
    // ---------------------------------------

    int event, ret;
    bool info = false;
    std::string out_file, dat_file;
    std::vector<float> dat_test, xpeaks, ypeaks;
    std::vector<float> dat;

    dat_file = path + folder + "/" + fn + "_Ch3.dat";
    ret = read_array(dat_file, ' ', dat, false);
    std::cout << "-- Reading data file: " << dat_file << ". " << dat.size() << " entries." << std::endl;



    // Analysis
    // --------------------------------------

    auto vals_01 = std::make_shared<peakValues> ();
    auto vals_02 = std::make_shared<peakValues> ();

    std::cout << "-- Starting analysis " << std::endl;

	auto h_noise = std::make_shared<TH1F>("", "", nbins_noise, vmin_noise, vmax_noise);
	auto h_freq = std::make_shared<TH1F>("", "", 500, 0, 5000);
    auto h_amp = std::make_shared<TH1F>("", "", nbins_amp, vmin_amp, vmax_amp);
    auto h_charge = std::make_shared<TH1F>("", "", nbins_charge, vmin_charge, vmax_charge);
    auto h_trise = std::make_shared<TH1F>("", "", 100, 0, 1);
    
    auto h_dtgaus = std::make_shared<TH1F>("", "", nbins_time, vmin_time, vmax_time);
    auto h_dt15 = std::make_shared<TH1F>("", "", nbins_time, vmin_time, vmax_time);
    auto h_dt30 = std::make_shared<TH1F>("", "", nbins_time, vmin_time, vmax_time);
    auto h_dt45 = std::make_shared<TH1F>("", "", nbins_time, vmin_time, vmax_time);
    auto h_dt60 = std::make_shared<TH1F>("", "", nbins_time, vmin_time, vmax_time);
    
    auto m_sn_vs_dt30 = std::make_shared<TH2F>("", "", 100, 0, 100, nbins_time, vmin_time, vmax_time);
    auto m_sn_vs_dt45 = std::make_shared<TH2F>("", "", 100, 0, 250, nbins_time, vmin_time, vmax_time);
    auto m_sn_vs_dt60 = std::make_shared<TH2F>("", "", 100, 0, 250, nbins_time, vmin_time, vmax_time);
    
    // peak search
    int cnt_coinc = 0;
    int cnt_excpt = 0;
    float sn_1, sn_2, sn_eff;
    for (int i = 0; i < 1000; i++){
        try {
            auto frame = std::vector<float>(dat.begin() + i*1000, dat.begin() + i*1000 + 1000);
            
            if (i%100 == 0){
                out_file = res_path + folder + "/" + "wf_single_ch3_" + std::to_string(i) + "_" + fn + ".pdf";
                plotWaveform(frame, 0, out_file);
                out_file = res_path + folder + "/" + "wf_single_ch4_" + std::to_string(i) + "_" + fn + ".pdf";
                plotWaveform(frame, 0, out_file);
            }
    
            // vals_01 = analysePeak(frame, "pos", 0, 400);
            // vals_02 = analysePeak(frame, "pos", 600, 1000);
            
            
            vals_01 = analysePeak(frame, "pos", 0, 200);
            vals_02 = analysePeak(frame, "pos", 700, 800);
    
            if ((vals_01->amp > thr) && (vals_02->amp > thr)) {
    
                sn_1 = vals_01->amp / vals_01->noise;
                sn_2 = vals_02->amp / vals_02->noise;
                sn_eff = (sn_1*sn_2) / sqrt(pow(sn_1, 2) + pow(sn_1, 2));
    
                // fill histogrammes
                h_noise->Fill(vals_01->noise);
                h_amp->Fill(vals_01->amp);
                h_charge->Fill(vals_01->charge);
                h_trise->Fill(vals_01->trise);
                h_noise->Fill(vals_02->noise);
                h_amp->Fill(vals_02->amp);
                h_charge->Fill(vals_02->charge);
                h_trise->Fill(vals_02->trise);
    
                h_dtgaus->Fill(vals_01->tpeak - vals_02->tpeak + 25);
                h_dt15->Fill(vals_01->tlinear15 - vals_02->tlinear15 + 25);
                h_dt30->Fill(vals_01->tlinear30 - vals_02->tlinear30 + 25);
                h_dt45->Fill(vals_01->tlinear45 - vals_02->tlinear45 + 25);
                h_dt60->Fill(vals_01->tlinear60 - vals_02->tlinear60 + 25);
    
                m_sn_vs_dt30->Fill(sn_eff, vals_01->tlinear30 - vals_02->tlinear30 + 25);
                m_sn_vs_dt45->Fill(sn_eff, vals_01->tlinear45 - vals_02->tlinear45 + 25);
                m_sn_vs_dt60->Fill(sn_eff, vals_01->tlinear60 - vals_02->tlinear60 + 25);
    
                cnt_coinc++;
            }
        }
        catch (const std::out_of_range& e) {
            cnt_excpt++;
            cout << "An exception occurred." << '\n';
        }
    }
    
    // waveforms
    std::vector<float> x(5000);
    std::vector<float> errx(5000, 0);
    std::vector<float> erry(5000, 0);
    std::iota (std::begin(x), std::end(x), 0);
    for (int i = 0; i < 100; i++){
        auto peak_01 = std::vector<float>(dat.begin() + i*5000, dat.begin() + i*5000 + 5000);
        auto peak_02 = std::vector<float>(dat.begin() + i*5000, dat.begin() + i*5000 + 5000);
    }
    
    // fits
    auto res = std::shared_ptr<runResults> ();
    auto resf = std::shared_ptr<fitResult> ();
    
    resf = fitGauss(h_dtgaus, 0.1, -0.5, +0.5);
    resf = fitGauss(h_dt15, 0.1, -0.5, +0.5);
    resf = fitGauss(h_dt30, 0.1, -0.5, +0.5);
    resf = fitGauss(h_dt45, 0.1, -0.5, +0.5);
    resf = fitGauss(h_dt60, 0.1, -0.5, +0.5);
    
    resf = fitLandau(h_charge, 0.0, 0, 40);
    resf = fitLandau(h_amp, 0.0, 0, 0.5);
    
    auto gr_sn_vs_dt30 = std::make_shared<TGraphErrors> ();
    gr_sn_vs_dt30 = getProjectionFrom2DAlongY(m_sn_vs_dt30, 100, 0, 100, 50);
    auto gr_sn_vs_dt45 = std::make_shared<TGraphErrors> ();
    gr_sn_vs_dt45 = getProjectionFrom2DAlongY(m_sn_vs_dt45, 100, 0, 250, 50);
    auto gr_sn_vs_dt60 = std::make_shared<TGraphErrors> ();
    gr_sn_vs_dt60 = getProjectionFrom2DAlongY(m_sn_vs_dt60, 100, 0, 250, 50);
    
    
    
    // Print
    // ---------------------------------------
    
    styleGraph();
    out_file = res_path + folder + "/" + "h_noise_" + fn + ".pdf";
    printHist(h_noise, out_file, "", "noise (V)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_amp_" + fn + ".pdf";
    printHist(h_amp, out_file, "", "waveform amplitude (V)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_charge_" + fn + ".pdf";
    printHist(h_charge, out_file, "", "waveform integral (V*ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_trise_" + fn + ".pdf";
    printHist(h_trise, out_file, "", "rise time (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_freq_" + fn + ".pdf";
    printHist(h_freq, out_file, "", "time difference between hits (ns)", "# events",-1.0, -1.0, 1.2);
    
    out_file = res_path + folder + "/" + "h_dtgaus_" + fn + ".pdf";
    printHist(h_dtgaus, out_file, "", "dtgaus (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_dt15_" + fn + ".pdf";
    printHist(h_dt15, out_file, "", "dt15 (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_dt30_" + fn + ".pdf";
    printHist(h_dt30, out_file, "", "dt30 (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_dt45_" + fn + ".pdf";
    printHist(h_dt45, out_file, "", "dt45 (ns)", "# events",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "h_dt60_" + fn + ".pdf";
    printHist(h_dt60, out_file, "", "dt60 (ns)", "# events",-1.0, -1.0, 1.2);
    
    out_file = res_path + folder + "/" + "gr_sn_vs_dt30_" + fn + ".pdf";
    printGraph(gr_sn_vs_dt30, out_file, "", "sn_eff", "dt30 rms (ns)",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "gr_sn_vs_dt45_" + fn + ".pdf";
    printGraph(gr_sn_vs_dt45, out_file, "", "sn_eff", "dt45 rms (ns)",-1.0, -1.0, 1.2);
    out_file = res_path + folder + "/" + "gr_sn_vs_dt60_" + fn + ".pdf";
    printGraph(gr_sn_vs_dt60, out_file, "", "sn_eff", "dt60 (ns)",-1.0, -1.0, 1.2);
    
    out_file = res_path + folder + "/" + "m_sn_vs_dt30_" + fn + ".pdf";
    printMap(m_sn_vs_dt30, out_file, "", "sn_eff", "dt30 (ns)", "# events", -1.0, -1.0, 1.2, 1.4);
    out_file = res_path + folder + "/" + "m_sn_vs_dt45_" + fn + ".pdf";
    printMap(m_sn_vs_dt45, out_file, "", "sn_eff", "dt45 (ns)", "# events", -1.0, -1.0, 1.2, 1.4);
    out_file = res_path + folder + "/" + "m_sn_vs_dt60_" + fn + ".pdf";
    printMap(m_sn_vs_dt60, out_file, "", "sn_eff", "dt60 (ns)", "# events", -1.0, -1.0, 1.2, 1.4);
    
    // std::cout << "-- Analysed run " << folder << " Found: "
    //           << cnt_coinc << " coincidencent signals."
    //           << cnt_excpt << " exceptions."
    //           << std::endl;
    // std::cout << std::endl << std::endl << std::endl;

    return res;
}









Double_t fitLandauGauss( Double_t *x, Double_t *par ) {

  static int nn=0;
  nn++;
  static double xbin = 1;
  static double b1 = 0;
  if( nn == 1 ) {
    b1 = x[0];
    std::cout << "b1 = " << b1 << std::endl;
  }
  if( nn == 2 ) {
    std::cout << "b2 = " << x[0] << std::endl;
    xbin = x[0] - b1;// bin width needed for normalization
    std::cout << "xbin = " << xbin << std::endl;
  }

  // Landau:
  Double_t invsq2pi = 0.3989422804014;   // (2 pi)^(-1/2)
  Double_t mpshift  = -0.22278298;       // Landau maximum location

  // MP shift correction:
  double mpc = par[0] - mpshift * par[1]; //most probable value (peak pos)

  //Fit parameters:
  //par[0] = Most Probable (MP, location) parameter of Landau density
  //par[1] = Width (scale) parameter of Landau density
  //par[2] = Total area (integral -inf to inf, normalization constant)
  //par[3] = Gaussian smearing

  // Control constants
  Double_t np = 100.0;      // number of convolution steps
  Double_t sc =   5.0;      // convolution extends to +-sc Gaussian sigmas

  // Range of convolution integral
  double xlow = x[0] - sc * par[3];
  double xupp = x[0] + sc * par[3];

  double step = (xupp-xlow) / np;

  // Convolution integral of Landau and Gaussian by sum

  double sum = 0;
  double xx;
  double fland;

  for( int i = 1; i <= np/2; i++ ) {

    xx = xlow + ( i - 0.5 ) * step;
    fland = TMath::Landau( xx, mpc, par[1] ) / par[1];
    sum += fland * TMath::Gaus( x[0], xx, par[3] );

    xx = xupp - ( i - 0.5 ) * step;
    fland = TMath::Landau( xx, mpc, par[1] ) / par[1];
    sum += fland * TMath::Gaus( x[0], xx, par[3] );
  }

  return( par[2] * invsq2pi * xbin * step * sum / par[3] );
}

//----------------------------------------------------------------------
void fitlang(std::shared_ptr<TH1> h, float x_low, float x_up, float smear_low, float smear_up ) {

  if( h == NULL ){
    std::cout << " does not exist\n";
  }

  else{

    h->SetMarkerStyle(21);
    h->SetMarkerSize(0.8);
    h->SetStats(1);
    //gStyle->SetOptFit(111);//111 = chisq err par
    gStyle->SetOptFit(101);//101 = chisq and par

    gStyle->SetOptStat(11);

    gROOT->ForceStyle();

    double aa = h->GetEntries();//normalization

    // find peak:
    int ipk = h->GetMaximumBin();
    double xpk = h->GetBinCenter(ipk);
    double sm = xpk / 6.5; // sigma
    double ns = (smear_up + smear_low) / 2.; // noise

    // create a TF1 with the range from x0 to x9 and 4 parameters
    auto fitFcn = std::make_shared<TF1> ( "fitFcn", fitLandauGauss, x_low, x_up, 4);

    fitFcn->SetParName( 0, "peak" );
    fitFcn->SetParName( 1, "sigma" );
    fitFcn->SetParName( 2, "area" );
    fitFcn->SetParName( 3, "smear" );

    fitFcn->SetNpx(500);
    fitFcn->SetLineWidth(2);
    fitFcn->SetLineColor(kRed);

    // set start values:
    fitFcn->SetParameter( 0, xpk ); // peak position, defined above
    fitFcn->SetParameter( 1, sm ); // width
    fitFcn->SetParameter( 2, aa ); // area
    fitFcn->SetParameter( 3, ns ); // noise

    fitFcn->SetParLimits( 3, smear_low, smear_up ); // noise

    h->Fit("fitFcn", "R", "ep" );// R = range from fitFcn
    h->Draw("histepsame");  // data again on top

    std::cout << "Ndata = " << fitFcn->GetNumberFitPoints() << std::endl;
    std::cout << "Npar  = " << fitFcn->GetNumberFreeParameters() << std::endl;
    std::cout << "NDoF  = " << fitFcn->GetNDF() << std::endl;
    std::cout << "chisq = " << fitFcn->GetChisquare() << std::endl;
    std::cout << "prob  = " << fitFcn->GetProb() << std::endl;

   }
}


