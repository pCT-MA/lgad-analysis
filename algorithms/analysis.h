/********************************************************************
 * File: analysis.h
 * ------------------------
 *
 * Description:
 *
 * Version:
 * Author: Florian Pitters
 *
 *******************************************************************/

#ifndef __ANALYSIS_H_INCLUDED__
#define __ANALYSIS_H_INCLUDED__

#include "common.h"
#include "waveform.h"


// --------------------------------------------------------
// Methods
// --------------------------------------------------------


struct runResults{
	float freq1_mean;
	float freq1_err;
	float freq1_rms;
	float amp1_mean;
	float amp1_err;
	float amp1_rms;
	float charge1_mean;
	float charge1_err;
	float charge1_rms;
	float noise1_mean;
	float noise1_err;
	float noise1_rms;
	float trise1_mean;
	float trise1_err;
	float trise1_rms;

	float freq2_mean;
	float freq2_err;
	float freq2_rms;
	float amp2_mean;
	float amp2_err;
	float amp2_rms;
	float charge2_mean;
	float charge2_err;
	float charge2_rms;
	float noise2_mean;
	float noise2_err;
	float noise2_rms;
	float trise2_mean;
	float trise2_err;
	float trise2_rms;

	float dt10_mean;
	float dt10_err;
	float dt10_rms;
	float dt10_std;
	float dt15_mean;
	float dt15_err;
	float dt15_rms;
	float dt15_std;
	float dt20_mean;
	float dt20_err;
	float dt20_rms;
	float dt20_std;
	float dt25_mean;
	float dt25_err;
	float dt25_rms;
	float dt25_std;
	float dt30_mean;
	float dt30_err;
	float dt30_rms;
	float dt30_std;
	float dt35_mean;
	float dt35_err;
	float dt35_rms;
	float dt35_std;
	float dt45_mean;
	float dt45_err;
	float dt45_rms;
	float dt45_std;
	float dt60_mean;
	float dt60_err;
	float dt60_rms;
	float dt60_std;
	float dtgaus_mean;
	float dtgaus_err;
	float dtgaus_rms;

};


int analyse();
std::shared_ptr<runResults> analyse_parallel(std::string path, std::string folder, std::string fn, std::string species);
std::shared_ptr<runResults> analyse_series(std::string path, std::string folder, std::string fn, std::string species);

void fitlang(std::shared_ptr<TH1> h, float x_low, float x_up, float smear_low, float smear_up);
Double_t fitLandauGauss( Double_t *x, Double_t *par ) ;

#endif
